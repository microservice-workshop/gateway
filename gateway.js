const { ApolloServer } = require('apollo-server');
const { ApolloGateway,RemoteGraphQLDataSource } = require('@apollo/gateway');
const { tokentToUserData } = require("./Authorization");


class InjectHeader extends RemoteGraphQLDataSource {
    willSendRequest({ request, context }) {

        request.http.headers.set("user-id", context.userId);
        request.http.headers.set("username", context.username)
        request.http.headers.set("authorization", context.authorization)   
    }
}

const gateway = new ApolloGateway({
    serviceList: [
        { name: 'accounts', url: 'http://localhost:8080/query' },
        { name: 'todos', url: 'http://localhost:8081/query' },
    ],
    buildService({ name, url }) {
        return new InjectHeader({ url });
    }, 
});

const server = new ApolloServer({
    gateway,
    playground: true,
    subscriptions: false,
    context: async ({ req }) => {
        const token = req.headers.authorization || "";
        let userData = await tokentToUserData(token)

        const data = {
            ...userData,
            ...req.headers,
        }

        return data;
    },
});

server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});
