const { ApolloError } = require("apollo-server");
var jwt = require('jsonwebtoken');

const decodeToken = function(token){
    var splitedToken = token.split(" ")

    if (splitedToken[0] != "Bearer") {
        throw new ApolloError("Invalid Token", "UNAUTHORIZED")
    }

    var decodedJwt = jwt.decode(splitedToken[1])
    if (decodedJwt == null) {
        throw new ApolloError("Invalid Token", "UNAUTHORIZED")
    }

    return decodedJwt
}

const tokentToUserData = async function (token) {
    let userId = "";
    let username = "";

    if (token !== "") {
        let decodedData = decodeToken(token);
        userId = decodedData.user_id;
        username = decodedData.username;  
    }

    return {
        userId: userId,
        username: username
    };
}

module.exports = {
    tokentToUserData: tokentToUserData
}